//#include "./h_file/stdio.h"
//#include "./h_file/stdlib.h"
//#include "./h_file/math.h"
//#include "./h_file/string.h"
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h> 

int main(int argc, char *argv[]){
  char *filename;
  if (argc < 2)
   {
        printf("Missing Filename\n");
        return(1);
   }
   else
   {
        filename = argv[1];
        printf("Filename : %s\n", filename);
   }

    FILE *myFile,*fp;

    //read lines
    fp = fopen(filename, "r");
    int lines=0;
    char ch;
    while((ch=fgetc(fp))!=EOF)
    {
    if (ch=='\n') { lines++; }
    }
    fclose(fp);
    printf("lines: %d\n",lines);

    //read file into array
    myFile = fopen(filename, "r");
    float allWater[lines][2];
    int i,j;

    for (i = 0; i < lines; i++)
    {
        fscanf(myFile, "%f %f %f", &allWater[i][0],&allWater[i][1],&allWater[i][2]);
    }      


        fclose(myFile);
// reduce water, box center 2 2 2
   int reW_num = 0;
   for (i = 0; i < lines; i++)
   {
	if (allWater[i][0]<5 && allWater[i][1]<5 && allWater[i][2]<5)
	{
		reW_num +=1;
	}
    }
   float reWater[reW_num][2];
   j = 0;
   for (i = 0; i < lines; i++)
   {
        if (allWater[i][0]<5 && allWater[i][1]<5 && allWater[i][2]<5)
        {
                reWater[j][0] = allWater[i][0];
		reWater[j][1] = allWater[i][1];
		reWater[j][2] = allWater[i][2];
		j +=1;
        }
    }
//  printf("%f %f %f\n", reWater[0][0],reWater[0][1],reWater[0][2]);  
   
    int grid_num = 0.8/0.1; //grid in 1D 
 // get the smallest edges of the box 2.3800  2.92  2.54
    float grid_edge_min[2];
    grid_edge_min[0] = 2.38-grid_num/2*0.1;
    grid_edge_min[1] = 2.92-grid_num/2*0.1;
    grid_edge_min[2] = 2.54-grid_num/2*0.1;
 // for loop to asign counts of each grid
    int x,y,z;
    int count[20][20][20] = {0};
    for (i = 0; i < reW_num; i++)
    {
	for (x = 0; x < grid_num; x++)
	{
		if (reWater[i][0]>(grid_edge_min[0]+x*0.1) && reWater[i][0]<(grid_edge_min[0]+(x+1)*0.1))
		{
			for (y = 0; y < grid_num; y++)
			{
				if (reWater[i][1]>(grid_edge_min[1]+y*0.1) && reWater[i][1]<(grid_edge_min[1]+(y+1)*0.1))
				{
					for (z = 0; z < grid_num; z++)
					{
						if (reWater[i][2]>(grid_edge_min[2]+z*0.1) && reWater[i][2]<(grid_edge_min[2]+(z+1)*0.1))
						{
							count[x][y][z] +=1;
						}
					}
				}
			}
		}
	}
    }
// testing print the max count
    int max_count=0;
    int max_count_id[3];
    for (x = 0; x < grid_num; x++)
    {
      for (y = 0; y < grid_num; y++)
      {
              for (z = 0; z < grid_num; z++)
              {
                      if (count[x][y][z]>max_count)
                      {
                              max_count=count[x][y][z];
                              max_count_id[0]=x;
                              max_count_id[1]=y;
                              max_count_id[2]=z;
                      	      printf("max count: %d in grid: %f %f %f\n", max_count, max_count_id[0]*0.1+grid_edge_min[0],
										     max_count_id[1]*0.1+grid_edge_min[1],
										     max_count_id[2]*0.1+grid_edge_min[2]);
			}
              }
      }
    }


    int valid_count=0;
    for (x = 0; x < grid_num; x++)
    {
      for (y = 0; y < grid_num; y++)
      {
              for (z = 0; z < grid_num; z++)
              {
                      if (count[x][y][z]>1000)
                      {
			 valid_count +=1;
                      }
              }
      }
    }
    
    int temp_count = 0;
    float valid_grid[valid_count][4];
    for (x = 0; x < grid_num; x++)
    {
      for (y = 0; y < grid_num; y++)
      {
              for (z = 0; z < grid_num; z++)
              {
                      if (count[x][y][z]>1000)
                      {
                        valid_grid[temp_count][0]=grid_edge_min[0]+x*0.1;
			valid_grid[temp_count][1]=grid_edge_min[1]+y*0.1;
			valid_grid[temp_count][2]=grid_edge_min[2]+z*0.1;
			valid_grid[temp_count][3]=count[x][y][z];
			temp_count +=1;
                      }
              }
      }
    }
    
    for (i = 0; i < valid_count; i++)
    {
	printf("counts:%f in %f %f %f\n",valid_grid[i][3],valid_grid[i][0],valid_grid[i][1],valid_grid[i][2]);
    }
   
    int itera_num = 2, k, iter_temp_count;
    float sumx, sumy, sumz, avex, avey, avez;
    float itera_grid_cor[valid_count][3];
    itera_grid_cor[valid_count][3]=valid_grid[valid_count][3]; // first iteration
    for (i = 0; i < itera_num; i ++)
    {
	for (j = 0; j < valid_count; j++)
	{       
		sumx = 0;
		sumy = 0;
		sumz = 0;
		avex = 0;
		avey = 0;
		avez = 0;
		iter_temp_count = 0;

		for (k = 0; k < reW_num; k ++)
		{
			if (sqrt(pow(reWater[k][0]-valid_grid[j][0],2)+
				 pow(reWater[k][1]-valid_grid[j][1],2)+
				 pow(reWater[k][2]-valid_grid[j][2],2)) < 0.6 )
			{
				iter_temp_count +=1;
				sumx = sumx + reWater[k][0];
				sumy = sumy + reWater[k][1];
				sumz = sumz + reWater[k][2];
			}
		}
		avex = sumx / iter_temp_count;
		avey = sumy / iter_temp_count;
		avez = sumz / iter_temp_count;
		itera_grid_cor[j][0] = avex;
		itera_grid_cor[j][1] = avey;
		itera_grid_cor[j][2] = avez;
	}
    }
//  print water site
    for (i = 0; i < valid_count; i ++)
    {
	printf("%f %f %f\n",itera_grid_cor[i][0],itera_grid_cor[i][1],itera_grid_cor[i][2]);
    }
// get the initial grids which have 

//
//
//// // for loop to asign the grid points
////    int x,y,z;
////    float grid_cor[8000][3];
////    int temp = 0;
////    for (x = 0; x < grid_num; x++)
////    {
////	for (y = 0; y < grid_num; y++)
////	{
////		for (z = 0; z < grid_num; z++)
////		{
////			grid_cor[temp][0]=grid_edge_min[0]+x*0.1;
////			grid_cor[temp][1]=grid_edge_min[1]+y*0.1;
////			grid_cor[temp][2]=grid_edge_min[2]+z*0.1;
////			temp += 1;
////		}
//	}
//    }
//    
//    int count[8000] = {0};
//    for (i = 0; i < lines; i++)
//	{
//		for (j = 0; j < 8000; j++)
//		{
//			if (abs(numberArray[i][0] - grid_cor[j][0])<=0.05)
//			{
//				if (abs(numberArray[i][1] - grid_cor[j][1])<=0.05)
//				{
//					if (abs(numberArray[i][2] - grid_cor[j][2])<=0.05)
//					{
//						count[j] +=1;
//					}
//				}
//			}
//		}
//	}
////    int max_count=0;
//    int max_count_id[2];				
//    for (x = 0; x < grid_num; x++)
//    {
//	for (y = 0; y < grid_num; y++)
//	{
//		for (z = 0; z < grid_num; z++)
//		{
//			if (count[x][y][z]>max_count)
//			{
//				max_count=count[x][y][z];
//				max_count_id[0]=x;
//				max_count_id[1]=y;
//				max_count_id[2]=z;
//			}
//		}
//	}
//    }
//    printf("max count: %d in grid: %d %d %d", max_count, max_count_id[0], max_count_id[1],max_count_id[2]);	
////print the coordinates if the counts larger than a certain number
//     int good_grid_num=0;
//     for (x = 0; x < grid_num; x++)
//     {
//	for (y = 0; y < grid_num; y++)
//	{
//		for (z = 0; z < grid_num; z++)
//		{
//			if (count[x][y][z] >= 100000)  //change the number here
//			{
//				good_grid_num +=1;	
//				//printf("counts: %d in %f %f %f\n",count[x][y][z],grid_edge_min[0]+x*0.12,grid_edge_min[1]+y*0.12,grid_edge_min[2]+z*0.12);
//			}
//		}
//	}
//     }
//     printf("good_grid number: %d\n",good_grid_num);
//     float good_grid_cor[good_grid_num][2]; //the fourth column is the number of counts
//     int temp_count=0;
//     //good_grid_cor[good_grid_num][3] = {0};
//     memset(good_grid_cor, 0, sizeof good_grid_cor);
//     for (x = 0; x < grid_num; x++)
//     {
//        for (y = 0; y < grid_num; y++)
//        {
//                for (z = 0; z < grid_num; z++)
//                {
//                        if (count[x][y][z] >= 100000)  //change the number here
//                        {
//				good_grid_cor[temp_count][0]=grid_edge_min[0]+x*0.12;
//				good_grid_cor[temp_count][1]=grid_edge_min[1]+y*0.12;
//				good_grid_cor[temp_count][2]=grid_edge_min[2]+z*0.12;
//                               	temp_count +=1;
//                        }
//                }
//        }
//     }
//    int temp_count2,num_itera,temp_count3;
//    float sumx, sumy, sumz, avex,avey,avez;
//    float itera_grid_cor[good_grid_num][2];
//    itera_grid_cor[good_grid_num][2]  = good_grid_cor[good_grid_num][2];
//    int itera_grid_num[good_grid_num];
//    memset(itera_grid_num,0,sizeof itera_grid_num);
//    int temp_grid_num = good_grid_num; // first iteration
//    for (num_itera=0;num_itera<20;num_itera++)
//    {	
//	temp_count3 = 0;    
//    	for (j=0;j<=temp_grid_num;j++)
//    	{
//		sumx = 0.0;
//        	sumy = 0.0;
//        	sumz = 0.0;
//		temp_count2=0;
//    		for (i = 0; i < lines; i++)
//    		{
//			if (abs(numberArray[i][0]-itera_grid_cor[j][0])<0.05 && 
//		    	    abs(numberArray[i][1]-itera_grid_cor[j][1])<0.05 &&
//		    	    abs(numberArray[i][2]-itera_grid_cor[j][2])<0.05)
//			{			
//				sumx=sumx+numberArray[i][0];
//				sumy=sumy+numberArray[i][1];
//				sumz=sumy+numberArray[i][2];
//				temp_count2 +=1;
//			}
//		avex = sumx/temp_count2;
//		avey = sumy/temp_count2;
//		avez = sumz/temp_count2;
//		}
//	        if (temp_count2 >=10000)
//		{
//			itera_grid_cor[temp_count3][0] = avex;
//			itera_grid_cor[temp_count3][1] = avey;
//			itera_grid_cor[temp_count3][2] = avez;
//			itera_grid_num[temp_count3] = temp_count2;
//		}
//		temp_grid_num = temp_count3;
//    	}
//		
//    }
//    int max=0;
//    int max_ind;
//    for (j=0;j<temp_count3;j++)
//    {
//	if (itera_grid_num[j] >= max)
//	{
//		max = itera_grid_num[j];
//		max_ind=j;
//	}
//    }
//    printf("max_counts: %d x,y,z is %f %f %f\n",max,itera_grid_cor[max_ind][0],itera_grid_cor[max_ind][1],itera_grid_cor[max_ind][2]);			
    return 0;
}
