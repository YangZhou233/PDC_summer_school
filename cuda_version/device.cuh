#define BLOCK_SIZE 1024
#define MAXWAT 500000000 // watnum * frames
#ifndef _DEVICE_H
#define _DEVICE_H
#endif

#define CHECK(call)                                                            \
{                                                                              \
    const cudaError_t error = call;                                            \
    if (error != cudaSuccess)                                                  \
    {                                                                          \
        fprintf(stderr, "Error: %s:%d, ", __FILE__, __LINE__);                 \
        fprintf(stderr, "code: %d, reason: %s\n", error,                       \
                cudaGetErrorString(error));                                    \
        exit(1);                                                               \
    }                                                                          \
}

#define arrayInit(array, size)           \
{                                        \
   for (int i=0;i<size;i++)              \
   {                                     \
      array[i] = 0;                      \
   }                                     \
}

typedef struct {
   int fid; //frameID
   int nid; //Wat counting ID at each frame!
   int aid; //original atom ID in topology
   double x;
   double y;
   double z;
} awt; // big structure of all water molecules among all frames

struct lesswater { int num;};
typedef struct lesswater lt;

extern void gpuCount(int * count, float * xnew, float * ynew, float * znew, int len_awt, float xs_min, float ys_min, float zs_min, float xs_max, float ys_max, float zs_max, int frames);
extern void gpuStructCpy(awt * mywat, int len_awt, int frames);
extern void gpuFreeStruct();
