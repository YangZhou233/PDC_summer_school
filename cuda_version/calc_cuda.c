// A try for integration
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h> 
#include <memory.h>
#include "typstruct.h"

extern void gpuCount(int * count, float * xnew, float * ynew, float * znew, int len_awt, float xs_min, float ys_min, float zs_min, float xs_max, float ys_max, float zs_max, int frames);
extern void gpuStructCpy(awt * mywat, int len_awt, int frames);
extern void gpuFreeStruct();

int findwat(int len_awt, awt * mywat, lt * leswat, int * les_num, int watnum, int frames, float (*init_grid)[5], int * init_count, float box_center[3], float box_size[3])
{
   int i, j,k, x, y, z;
   int reW_num = 0;
   int ***count;
   float grid_edge_min[3] = {0};
   float occ; // Occupancy
   int max_count=0;
   int valid_count = 0;
   float fframe = (float) frames;
   int grid_num[3];

   /* 
    calculate simulation box size and center
   */
   
   grid_num[0] = (box_size[0]+GRID_SIZE)/GRID_SIZE ; // number of grids on dimonsion x
   grid_num[1] = (box_size[1]+GRID_SIZE)/GRID_SIZE ; // number of grids on dimonsion y
   grid_num[2] = (box_size[2]+GRID_SIZE)/GRID_SIZE ; // number of grids on dimonsion z


   count = (int***)calloc(grid_num[0],sizeof(int**));
   for(i=0;i<grid_num[0];i++)
   {
     count[i]=(int**)calloc(grid_num[1],sizeof(int*));
     for(j=0;j<grid_num[1];j++)
     {
       count[i][j]=(int*)calloc(grid_num[2],sizeof(int));
       for(k=0;k<grid_num[2];k++)
       {
         count[i][j][k]=0;
       }
     }
   }


//   int grid_num = (BOX_SIZE+GRID_SIZE)/GRID_SIZE ; // number of grids on one dimonsion
   int total_grids_number = grid_num[0]*grid_num[1]*grid_num[2]; //number of grids in a cubic box
   
   //init_grid[watnum][5]; //(about 6000 of water molecules in this example, be caution about the "OverStack flow"!)

   if (len_awt - 1 != watnum * (frames - 1)) {
      printf("Error in reading xtc and pdb!\n");
      printf("watnum is %d, len_awt is %d, frames is %d\n", watnum, len_awt, frames);
      printf("is there too many frames? at present, watnum * frames should be less than 10^9\n\n");
      exit(1);
   }

// reduce water with a box size around the box_center
   
   for (i = 1; i < len_awt; i++) {
      if (mywat[i].x < box_center[0]+box_size[0]/2.0 && 
          mywat[i].x > box_center[0]-box_size[0]/2.0 && 
          mywat[i].y < box_center[1]+box_size[1]/2.0 && 
          mywat[i].y > box_center[1]-box_size[1]/2.0 && 
          mywat[i].z < box_center[2]+box_size[2]/2.0 &&
          mywat[i].z > box_center[2]-box_size[2]/2.0 ) 
      {
         (leswat+reW_num)->num = i;
         reW_num +=1;
      }
   }

   *les_num = reW_num;
   grid_edge_min[0] = box_center[0] - grid_num[0] / 2 * GRID_SIZE;
   grid_edge_min[1] = box_center[1] - grid_num[1] / 2 * GRID_SIZE;
   grid_edge_min[2] = box_center[2] - grid_num[2] / 2 * GRID_SIZE;

    
   for (i = 0; i < reW_num; i++) {
      //j = leswat[i];
      j = (leswat+i)->num;
      //printf("i(reW_num) is %d, J is %d\n", i, j);
      x = (int)((mywat[j].x - grid_edge_min[0])/GRID_SIZE);
      y = (int)((mywat[j].y - grid_edge_min[1])/GRID_SIZE);
      z = (int)((mywat[j].z - grid_edge_min[2])/GRID_SIZE);
      if (x < grid_num[0] && y < grid_num[1] && z < grid_num[2]) {
         count[x][y][z] += 1;
      }
   }
   
// testing print the max count

   for (x = 0; x < grid_num[0]; x++) {
      for (y = 0; y < grid_num[1]; y++) {
         for (z = 0; z < grid_num[2]; z++) {
            if (count[x][y][z]>max_count) {
               max_count=count[x][y][z];
               // To recover the center's xyz from edge should be grid_edge_min+x*0.12+0.06; because length of the grid edge is 0.12

               printf("max count: %d in grid center: %f %f %f\n", max_count, x * GRID_SIZE  + grid_edge_min[0] + GRID_SIZE / 2.0, y * GRID_SIZE + grid_edge_min[1] + GRID_SIZE / 2.0, z * GRID_SIZE + grid_edge_min[2] + GRID_SIZE / 2.0);
            }
            float fcount = (float) count[x][y][z];
	    
            if (fcount > fframe * 0.2) {
               // The init grid here is still the closest point to min_edge, not the center, needs to plus 0.06!;
               // There should have a vector to store the counting ID: x, y, z!
               init_grid[valid_count][0] = grid_edge_min[0]+x*GRID_SIZE + GRID_SIZE / 2.0;
               init_grid[valid_count][1] = grid_edge_min[1]+y*GRID_SIZE + GRID_SIZE / 2.0;
               init_grid[valid_count][2] = grid_edge_min[2]+z*GRID_SIZE + GRID_SIZE / 2.0;
               init_grid[valid_count][3] = fcount;
               init_grid[valid_count][4] = 0.0; //The tag for merging (shoul be structure, for storing float and char!)
               valid_count +=1;
            }
         }
      }
   }

   *init_count = valid_count;
   //printf("valid_count grid_num: %d %d\n", valid_count, grid_num);
   for (i = 0; i < valid_count; i++) {
      occ = init_grid[i][3] / fframe * 100.0;
      printf("Initial hydration site %2d: %f %f %f (nm); Frames: %f, Occupancies: %f%\n", i, init_grid[i][0], init_grid[i][1], init_grid[i][2], init_grid[i][3], occ);
    }

for(i=0;i<grid_num[0];i++)
{
    for(j=0;j<grid_num[1];j++)
    {
        free(count[i][j]);
    }
}
for(i=0;i<grid_num[0];i++)
{
    free(count[i]);
}
free(count);


   return 0;
}

int merge(float (*init_grid)[5], int init_count, float (*merge_grid)[5], int * merge_count) {
   int i, j, k, temp_count;
   float xx, yy, zz, dist2;
   k = 0;
   for (i=0;i<init_count;i++) {
      if (DEBUG == 1) {
         printf("Checking if label changed: init_count ID %d and its label: %f\n", i, init_grid[i][4]);
      }
      if (init_grid[i][4] == 0.0) {
         temp_count = 1;
         xx = init_grid[i][0];
         yy = init_grid[i][1];
         zz = init_grid[i][2];
         init_grid[i][4] = 1.0;

         for (j=0;j<init_count;j++) {
            dist2 = pow(init_grid[j][0] - xx, 2) + pow(init_grid[j][1] - yy, 2) + pow(init_grid[j][2] - zz, 2);
            if (dist2 < pow(0.1, 2) && dist2 > 0.0) { // max 6 adjacent sites, and excludes itself
               xx += init_grid[j][0];
               yy += init_grid[j][1];
               zz += init_grid[j][2];
               temp_count += 1;
               init_grid[j][4] = 1.0;
               if (DEBUG == 1) {
                  printf("Found %d adjacent site for the site %d; adding site ID %d to the %d th merged site\n", temp_count-1, i, j, k);
               }
            }
         }

         float tmp3 = (float) temp_count;
         xx /= tmp3;
         yy /= tmp3;
         zz /= tmp3;
         merge_grid[k][0] = xx;
         merge_grid[k][1] = yy;
         merge_grid[k][2] = zz;
	 merge_grid[k][3] = temp_count;
	 merge_grid[k][4] = 0.0;
         k += 1;
      }
   }
   *merge_count = k;
   if (DEBUG == 1) {
      printf("Initial count is %d; Merged count is %d\n", init_count, *merge_count);
   }
   return 0;
}

int iteration(awt * mywat, float (*init_grid)[5], int init_count, lt * leswat, int les_num, int len_awt, int frames) {
   int h, i, j, k, siteCount;
   int temp_count, merge_count;
   float xs_min, ys_min, zs_min, xs_max, ys_max, zs_max, occ;
   double tmpde, deviation;
   float merge_grid[6000][5] = {0};
   float new_grid[6000][5] = {0};
   float fframe = (float) frames;
   float (*ingr)[5];
   int * count;
   int val_count;
   float * xnew, * ynew, * znew;
   float val_xnew, val_ynew, val_znew;

   FILE * printsite = fopen("WatSites.pdb", "w");
   
   // Initial the GPU device and Copy the mywat to GPU device
   printf("check memsize3: sizeof(mywat) %d, sizeof(leswat) %d\n", sizeof(mywat), sizeof(leswat));
   printf("len_awt -2: %d; mywat[len_awt - 2].x is %f\n", len_awt - 2, mywat[len_awt - 2].x);
   printf("les_num -2: %d; (leswat+les_num-2)->num is %d\n", les_num-2, (leswat+les_num-2)->num);
   gpuStructCpy(mywat, len_awt, frames);
   
   //merge_count = init_count;
   ingr = init_grid;
   
   for (h=0;h<ITER_TIMES;h++) {
      deviation = 0.000;
      tmpde     = 0.000;
      if (DEBUG == 1) {
         printf("\n\n\n\nThis is a seperator for printing merge info. at round %d\n", h);
      }

      if (h == 0) {
         merge(ingr, init_count, merge_grid, &merge_count);
      }
      else {
         merge(ingr, temp_count, merge_grid, &merge_count);
      }
      if (DEBUG == 1) {
         printf("MERGE_COUNT after %d round of merge() is %d\n", h, merge_count);
      }

      siteCount = 0;
      for (i=0;i<merge_count;i++) {
         // The diameter here is 0.13, plus or minus CUTOFF/2
         xs_min = merge_grid[i][0] - CUTOFF / 2.0;
         ys_min = merge_grid[i][1] - CUTOFF / 2.0;
         zs_min = merge_grid[i][2] - CUTOFF / 2.0;
         xs_max = merge_grid[i][0] + CUTOFF / 2.0;
         ys_max = merge_grid[i][1] + CUTOFF / 2.0;
         zs_max = merge_grid[i][2] + CUTOFF / 2.0;
         val_xnew = 0.0;
         val_ynew = 0.0;
         val_znew = 0.0;
         val_count = 0;
         
         // Try to count number of wats on GPU
         xnew  = &val_xnew;
         ynew  = &val_ynew;
         znew  = &val_znew;
         count = &val_count;
         //printf("trying to run on GPU, at %d round, at %d site, *znew %f\n", h, i, *znew);
         gpuCount(count, xnew, ynew, znew, len_awt, xs_min, ys_min, zs_min, xs_max, ys_max, zs_max, frames);
                  
         if (h<=10) {
            printf("checking: *count, *xnew, *ynew, *znew: %d %f %f %f at round %d for site %d\n", *count, *xnew, *ynew, *znew, h, i);
         }

         float fcount = (float) *count;
         if (fcount > fframe * 0.2) {
            *xnew /= *count;
            *ynew /= *count;
            *znew /= *count;
            tmpde = sqrt(pow(*xnew - merge_grid[i][0], 2) + pow(*ynew - merge_grid[i][1], 2) + pow(*znew - merge_grid[i][2], 2));
            
            if (tmpde > deviation) {
               deviation = tmpde;
            }
            
            new_grid[i][0] = *xnew;
            new_grid[i][1] = *ynew;
            new_grid[i][2] = *znew;
	    new_grid[i][3] = fcount;
            new_grid[i][4] = 0.0;
            siteCount += 1;
            //printf("TEMP Hydration site %2d: %f %f %f (nm); Frames: %f, at round %d\n", i, new_grid[i][0], new_grid[i][1], new_grid[i][2], new_grid[i][3], h);
         }
      }
      
      if (siteCount == 0) {
         printf("Warning: There are no desired water sites found in the %d th round's iteration\n", h);
         printf("The deviation is therefore being %f\n\n", deviation);
         exit(0);
      }
      // Here the deviation should not be the average value. It should be compared to the largest distance:
      else if (deviation < pow(0.1, 3)) {
         printf("The max deviation (%f) reached the drifting creterion of 0.001 nm after %d rounds of iteration\n", deviation, h);
         break;
      }
      
      ingr = new_grid;
      temp_count = i;
      if (DEBUG == 1) {
         printf(" %d sites were found at %d round, max_deviation is %lf\n", temp_count, h, deviation);
      }
   } // Ending of the iteration
   
   for (i = 0; i < siteCount; i ++)
   {
      occ = new_grid[i][3] / fframe * 100.0;
      printf("Final hydration site %2d: %f %f %f (nm); Frames: %f, Occupancies: %f%\n", i, new_grid[i][0], new_grid[i][1], new_grid[i][2], new_grid[i][3], occ);
      
      fprintf(printsite, "HETATM%5d O    H2O%6d      %5.3f  %5.3f  %5.3f %4.2f %4.2f           O-1\n",i,i,new_grid[i][0]*10,new_grid[i][1]*10,new_grid[i][2]*10,1.0,occ);
   }
   gpuFreeStruct();
   return 0;
}

