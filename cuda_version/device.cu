extern "C" {
#include "device.cuh"
}
#include <cuda_runtime.h>
#include <iostream>
#include <stdio.h>
#include <math.h>

__device__ awt * d_mywat;
__device__ __constant__ int * dcount;
__device__ __constant__ float * xN, * yN, * zN;

//Kernel
__global__ void countOnGpu(awt * d_mywat, int * dcount, float * xN, float * yN, float * zN, int len_awt, float xs_min, float ys_min, float zs_min, float xs_max, float ys_max, float zs_max) {
   int j = threadIdx.x + blockDim.x * blockIdx.x;

   int index;
   if (j>=len_awt) return;

   if (d_mywat[j].x > xs_min && d_mywat[j].x < xs_max \
               && d_mywat[j].y > ys_min && d_mywat[j].y < ys_max \
               && d_mywat[j].z > zs_min && d_mywat[j].z < zs_max) {
      index = d_mywat[j].fid;
      xN[index] = d_mywat[j].x;
      yN[index] = d_mywat[j].y;
      zN[index] = d_mywat[j].z;
      dcount[index] = 1;
   }
}


extern void gpuCount(int * count, float * xnew, float * ynew, float * znew, int len_awt, float xs_min, float ys_min, float zs_min, float xs_max, float ys_max, float zs_max, int frames) {
   int c_fid[frames+1];
   float x_fid[frames+1], y_fid[frames+1], z_fid[frames+1];
   arrayInit(c_fid, frames+1);
   arrayInit(x_fid, frames+1);
   arrayInit(y_fid, frames+1);
   arrayInit(z_fid, frames+1);

   int c_sum = 0;
   float x_sum = 0.0, y_sum = 0.0, z_sum = 0.0;
   dim3 gridSize((len_awt + (BLOCK_SIZE - 1)) / BLOCK_SIZE);
   //printf("gridSize is: %d\n", (len_awt + (BLOCK_SIZE - 1)) / BLOCK_SIZE);
   dim3 blockSize(BLOCK_SIZE);
   
   cudaEvent_t start, stop;
   cudaEventCreate(&start);
   cudaEventCreate(&stop); 
   
   CHECK(cudaMemcpy(dcount, c_fid, sizeof(int)*(frames+1), cudaMemcpyHostToDevice));
   CHECK(cudaMemcpy(xN, x_fid, sizeof(float)*(frames+1), cudaMemcpyHostToDevice));
   CHECK(cudaMemcpy(yN, y_fid, sizeof(float)*(frames+1), cudaMemcpyHostToDevice));
   CHECK(cudaMemcpy(zN, z_fid, sizeof(float)*(frames+1), cudaMemcpyHostToDevice));

   cudaEventRecord(start);
   countOnGpu<<<gridSize, blockSize>>>(d_mywat, dcount, xN, yN, zN, len_awt, xs_min, ys_min, zs_min, xs_max, ys_max, zs_max);
   CHECK(cudaDeviceSynchronize());

   cudaEventRecord(stop);

   CHECK(cudaMemcpy(c_fid, dcount, sizeof(int)*(frames+1), cudaMemcpyDeviceToHost));
   CHECK(cudaMemcpy(x_fid, xN, sizeof(float)*(frames+1), cudaMemcpyDeviceToHost));
   CHECK(cudaMemcpy(y_fid, yN, sizeof(float)*(frames+1), cudaMemcpyDeviceToHost));
   CHECK(cudaMemcpy(z_fid, zN, sizeof(float)*(frames+1), cudaMemcpyDeviceToHost));
   cudaEventSynchronize(stop);
   
   // cpuSum, which could be replaced by cuda reduction
   for (int i=0;i<=frames;i++) {
      c_sum += c_fid[i];
      x_sum += x_fid[i];
      y_sum += y_fid[i];
      z_sum += z_fid[i];
   }
   
   memcpy(count, &c_sum, sizeof(int));
   memcpy(xnew, &x_sum, sizeof(float));
   memcpy(ynew, &y_sum, sizeof(float));
   memcpy(znew, &z_sum, sizeof(float));
   
   //printf("check box size: xs_min is %f, xs_max is %f, ys_min is %f, ys_max is %f\n", xs_min, xs_max, ys_min, ys_max);
   //printf("CHECK result: x_sum %f, y_sum %f, z_sum %f, count %d\n\n", x_sum, y_sum, z_sum, c_sum);
   //size_t milliseconds;
   //cudaEventElapsedTime(&milliseconds, start, stop);
   //printf("Effective Bandwidth (GB/s): %u \n\n",  len_awt/milliseconds/1e6);
   
   CHECK(cudaGetLastError());
   //CHECK(cudaDeviceReset());
}

extern void gpuStructCpy(awt * mywat, int len_awt, int frames) {
   int dev = 0;
   size_t mywat_size = sizeof(awt) * len_awt;

   int i = len_awt - 2;
   printf("mywat size %u\n", mywat_size);
   printf("i, %d, mywat[i].x is %f, mywat[5].x is %f\n", i, mywat[i].x, mywat[5].x);
   CHECK(cudaSetDevice(dev));

   CHECK(cudaMalloc((int**)&dcount, sizeof(int)*(frames+1)));
   CHECK(cudaMalloc((float**)&xN, sizeof(float)*(frames+1)));
   CHECK(cudaMalloc((float**)&yN, sizeof(float)*(frames+1)));
   CHECK(cudaMalloc((float**)&zN, sizeof(float)*(frames+1)));
   //CHECK(cudaMalloc((awt**)&d_mywat, mywat_size));
   CHECK(cudaMalloc((void**)&d_mywat, mywat_size));
   CHECK(cudaMemcpy(d_mywat, mywat, mywat_size, cudaMemcpyHostToDevice));
}
   
extern void gpuFreeStruct() {
   CHECK(cudaFree(d_mywat));
   CHECK(cudaFree(dcount));
   CHECK(cudaFree(xN));
   CHECK(cudaFree(yN));
   CHECK(cudaFree(zN));
}
