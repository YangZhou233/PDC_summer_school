#define LINELEN_MAX 120
#define MAXCHAR 200
#define MAXATOM 200000
#define MAXWAT 100000000
#define MAXFRAME 200000
// SOME fixed parameters are defined here, no need to pass them
#define GRID_SIZE 0.120000
#define ITER_TIMES 100
#define CUTOFF 0.1300000
#define DEBUG 1 // debug or verbose mode, levev 0-2
//#define BOX_SIZE 3.200000

#include <string.h>
#include <omp.h>

struct VECTOR {
   double v1;
   double v2;
   double v3;
};
typedef struct VECTOR vt;

struct lesswater { int num;};
typedef struct lesswater lt;

typedef struct {
   int id;
   char name[10];
   char aa[20];
   int resno;
   double x;
   double y;
   double z;
} ATOM;

typedef struct { 
   int id;   // original ID in topology
   int neid; // counting ID
   double x;
   double y;
   double z;
} wl; // water_id_list for each frame // aborted


typedef struct {
   int frameid;
   int step;
   float time;
   //double bocx;
   //double bocy;
   //double bocz; // other way to store the box
   wl * watom; // The pointer for the structure in a structure seems to be overlaped by the last frame, aborted!
} WAT;

typedef struct {
   int fid; //frameID
   int nid; //Wat counting ID at each frame!
   int aid; //original atom ID in topology
   double x;
   double y;
   double z;
} awt; // big structure of all water molecules among all frames
   

/* The initial was taken inside a single code; uncomment if 
 * there are multiple code files
void initial(int num, ATOM * atom, char * resname)
{
        int i;
        for (i = 0; i < num; i++) {
                atom[i].resno = 1;
                strcpy(atom[i].name, "NN");
                strcpy(atom[i].aa, resname);
                //atom[i].x = 1.000000;
                //atom[i].y = 1.000000;
                //atom[i].z = 1.000000;
        }
}
*/
