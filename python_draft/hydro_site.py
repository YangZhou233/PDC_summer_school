import numpy as np
import time
import sys

#############################################################
#
#  useage: python hydro_site.py test.txt 1000
#
#############################################################
##
## input data format
##
#############################################################
#
# oxygen_coordinate = [ [ x1, y1, z1 ],
#                       [ x2, y2, z2 ],
#                       ......
#                       [ xn, yn, zn ] ]
#
#############################################################

start_time = time.time()

file = sys.argv[1]
#file='../../test.txt'
#file='test.txt'
#FRAMES=1000
FRAMES = sys.argv[2]
oxygen_coordinate = np.loadtxt(file)


##############################################################
#
# Define a box to cover the center that we are interested
# e.g. the size of the box is sx*sy*sz nm around [ x y z ]
# test_center="3.0238 3.1823 2.6268" #nm 
#
##############################################################
#
# box_center= [ x, y, z ]
#
# box_size = [ sx, sy, sz ] #nm
#
#############################################################


test_center = "23.800  29.199  25.353" #angstrom
box_center=[ float(x)/10 for x in test_center.split() ]
box_size=[ 0.5, 0.5, 0.5 ]




##############################################################
#
#  remove the oxygen_coordinate not in box
#
##############################################################


oxgen_coordinate_inbox=[]
for i in oxygen_coordinate:
    if box_center[0]-box_size[0]<i[0]<box_center[0]+box_size[0]:
        if box_center[1]-box_size[1]<i[1]<box_center[1]+box_size[1]:
            if box_center[2]-box_size[2]<i[2]<box_center[2]+box_size[2]:
                oxgen_coordinate_inbox.append(i)
oxgen_coordinate=np.array(oxgen_coordinate_inbox)




#############################################################
#
# Step 1
# Gridding the box using grids with width of 0.12 nm
#
#############################################################
#
#
#grids_size = 0.12 #nm
#
#grids_number_x = sx/grid_size
#
#grids_centers_x = np.linspace(x-0.5*sx, x+0.5*sx, grids_number)
#
#
#
## Output: initialize empty grid
#grids_array = {[x1, y1, z1]: 0,
#               [x2, y2, z2]: 0,
#                 ......
#               [xn, yn, zn]: 0}
#
############################################################

grid_size = 0.12 #nm

grids_number = [ int((x+grid_size)/grid_size) for x in box_size ]

grids_centers_x = np.linspace(box_center[0] - 0.5*box_size[0],\
                              box_center[0] + 0.5*box_size[0],\
                              grids_number[0])


grids_centers_y = np.linspace(box_center[1] - 0.5*box_size[1],\
                              box_center[1] + 0.5*box_size[1],\
                              grids_number[1])


grids_centers_z = np.linspace(box_center[2] - 0.5*box_size[2],\
                              box_center[2] + 0.5*box_size[2],\
                              grids_number[2])

grids_array = { (x, y, z):0 for x in grids_centers_x \
                            for y in grids_centers_y \
                            for z in grids_centers_z }


##############################################################
##
## Step 2
## Assigned the water molecules in each frame of the 
## simulation trajectories to the grids
##
##############################################################
#
#
#for x y z in oxygen_coordinate:
#    for grid in grids_array:
#        if x y z in grid.key():
#            grid.value()+=1
#    
## Output: initialized grids with water numbers
#grids_array = {[x1, y1, z1]: n1,
#               [x2, y2, z2]: n2,
#                 ......
#               [xn, yn, zn]: n3}
#
##############################################################

for grid in grids_array:
    xscale=(grid[0]-0.5*box_size[0],grid[0]+0.5*box_size[0])
    yscale=(grid[1]-0.5*box_size[1],grid[1]+0.5*box_size[1])
    zscale=(grid[2]-0.5*box_size[2],grid[2]+0.5*box_size[2])
    for x,y,z in oxygen_coordinate:
        if(xscale[0]<x<xscale[1])&\
          (yscale[0]<y<yscale[1])&\
          (zscale[0]<z<zscale[1]):
            grids_array[grid]+=1


##############################################################
##
## Step 3
## If the grid is occupied by a water molecule in more 
## than 20% of the frames, such a grid is defined as the 
## initial hydration site.
##
##############################################################
#
#
#for grid in grid_array:
#    if grid.value() > 0.2:    #20%
#        initail_hydration_sites.append(grid.key())
#
#
## Output initialized hydration sites
#initail_hydration_sites = [ [x1, y1, z1],
#                            [x2, y2, z2],
#                               ......
#                            [xn, yn, zn] ]
#                       
##############################################################

DENSITY=0.2
DENSITY_NUM=FRAMES*DENSITY

initail_hydration_sites = [ grid \
                            for grid, val in grids_array.items() \
                            if val > DENSITY_NUM ]



##############################################################
##
## Step 4
## Carry out iterations by calculating the geometric center 
## of the water molecules in the initial hydration site along 
## the trajectory until the drift of center is less than 0.01 
## angstrom or the 20 times of iteration is reached.
## water molecules within 1.3 angstrom of the geometric center 
## are assumed to be the water molecules in this hydration site.
##
## On the fly of the iteration, two center are merged into one 
## if the distance between two centers is less than 1.0 angstrom 
## 
##############################################################
#
#
#def average_xyz(inp_xyz, coordinates):
#    ...... 
#    return [ x, y, z ]
#
#
#iteration_times=20
#
#for site in initail_hydration_sites:
#    if (new_site - site > 0.01) or (count < iteration_times):
#        new_site = average_xyz(site)
#        count++
#
#
##Output final result
#hydration_sites = [ x1, y1, z1, n1,
#                    x2, y2, z2, n2,
#                    ......
#                    xn, yn, zn, nn ]
#
##############################################################

cutoff = 0.13 #nm

hydration_sites=initail_hydration_sites

iteration_times=20

def distance(siteA, siteB):
    #return np.sqrt((siteA[0]-siteB[0])**2 + \
    #               (siteA[1]-siteB[1])**2 + \
    #               (siteA[2]-siteB[2])**2)
    return ((siteA[0]-siteB[0])**2 + \
                   (siteA[1]-siteB[1])**2 + \
                   (siteA[2]-siteB[2])**2)

def merge_close(hydration_sites):
    new_hydration_sites=[]
    tag_sites={coord:'not calculated' for coord in hydration_sites}
    for site, tag in tag_sites.items():
        if tag == 'not calculated':
            count = 1
            x,y,z = site[0],site[1],site[2]
            tag_sites[site] = 'calculated'
            for site2, tag2 in tag_sites.items():
                if distance(site,site2) < 0.1**2:
                    x+=site2[0]
                    y+=site2[1]
                    z+=site2[2]
                    count+=1
                    tag_sites[site2] = 'calculated'
            x/=count
            y/=count
            z/=count
            new_hydration_sites.append((x,y,z))
    return new_hydration_sites 



for _ in range(iteration_times):
    hydration_sites = merge_close(hydration_sites)
    new_hydration_sites=[]
    for site in hydration_sites:
        #cutoff/2
        xscale=(site[0]-cutoff/2.0,site[0]+cutoff/2.0)
        yscale=(site[1]-cutoff/2.0,site[1]+cutoff/2.0)
        zscale=(site[2]-cutoff/2.0,site[2]+cutoff/2.0)
        xnew,ynew,znew=0.0, 0.0, 0.0
        count=0
        for x,y,z in oxygen_coordinate:
            if(xscale[0]<x<xscale[1])&\
              (yscale[0]<y<yscale[1])&\
              (zscale[0]<z<zscale[1]):
                xnew+=x
                ynew+=y
                znew+=z
                count+=1
        if count > DENSITY_NUM:
            xnew/=count
            ynew/=count
            znew/=count
            new_hydration_sites.append((xnew,ynew,znew))
    hydration_sites=new_hydration_sites


###############################################################
#
#  write to pdb and test 
#
###############################################################

i=1
atomtype='HETATM'
atom='O'
resname='H2O'
for site in hydration_sites:
    print("HETATM%5d O    H2O%6d      %5.3f  %5.3f  %5.3f %4.3f   %2.1f           O-1" %(i,i,site[0]*10,site[1]*10,site[2]*10,1,1))
    i+=1


print("%f s" %(time.time() - start_time))

