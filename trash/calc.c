// A try for integration
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h> 
#include "typstruct.h"

//int rpdb(char * filename, int * atomnum, int * watnum, ATOM * atom);
//int rxtc(char * filename, int atomnum, int watnum, ATOM * atom, awt * mywat, vt * boxct, int * frames, int * len_awt);



int findwat(int len_awt, awt * mywat, int watnum, int frames)
{
   int h, i, j, x, y, z;
   int reW_num = 0;
   int leswat[len_awt];
   memset(leswat,0,sizeof leswat);
   int count[20][20][20] = {0}; // box size, need to change
   int grid_num = 0.8/0.1; //grid in 1D 
   float grid_edge_min[2];
//   int max_count=0;
//   int max_count_id[3];
   int valid_count=0;
   int temp_count = 0;
   float valid_grid[1000][4];
   int itera_num = 1, k, iter_temp_count;
   float sumx, sumy, sumz, avex, avey, avez;
   float itera_grid_cor[1000][2];

   
   for (i = 1; i < len_awt; i++)
   {
      if (mywat[i].x <5 && mywat[i].y<5 && mywat[i].z<5) // the range need to be changed
      {
         leswat[reW_num] = i;
         reW_num +=1;
	}
    }

    grid_edge_min[0] = 2.38-grid_num/2*0.1;
    grid_edge_min[1] = 2.92-grid_num/2*0.1;
    grid_edge_min[2] = 2.54-grid_num/2*0.1;
 // for loop to asign counts of each grid

   for (i = 0; i < reW_num; i++) {
      j = leswat[i];
      for (x = 0; x < grid_num; x++) {
         if (mywat[j].x>(grid_edge_min[0]+x*0.1) && mywat[j].x<(grid_edge_min[0]+(x+1)*0.1)) {
            for (y = 0; y < grid_num; y++) {
               if (mywat[j].y>(grid_edge_min[1]+y*0.1) && mywat[j].y<(grid_edge_min[1]+(y+1)*0.1)) {
                  for (z = 0; z < grid_num; z++) {
                     if (mywat[j].z>(grid_edge_min[2]+z*0.1) && mywat[j].z<(grid_edge_min[2]+(z+1)*0.1)) {
                        count[x][y][z] +=1;
                     }
                  }
               }
            }
         }
      }
   }








   for (x = 0; x < grid_num; x++) {
      for (y = 0; y < grid_num; y++) {
         for (z = 0; z < grid_num; z++) {
            if (count[x][y][z]>100) {
               valid_count +=1;
            }
         }
      }
   }


    

    for (x = 0; x < grid_num; x++)
    {
      for (y = 0; y < grid_num; y++)
      {
              for (z = 0; z < grid_num; z++)
              {
                      if (count[x][y][z]>100)
                      {
                        valid_grid[temp_count][0]=grid_edge_min[0]+x*0.1;
						valid_grid[temp_count][1]=grid_edge_min[1]+y*0.1;
						valid_grid[temp_count][2]=grid_edge_min[2]+z*0.1;
						valid_grid[temp_count][3]=count[x][y][z];
						temp_count +=1;
                      }
              }
      }
    }



    
    for (i = 0; i < valid_count; i++)
    {
	printf("counts:%f in %f %f %f\n",valid_grid[i][3],valid_grid[i][0],valid_grid[i][1],valid_grid[i][2]);
    }


//    for (i = 0; i < reW_num; i++) {
//    printf("first: %d\n",leswat[i]);}

    //itera_grid_cor[valid_count][2]=valid_grid[valid_count][2]; // first iteration
    for (i = 0; i < valid_count; i++)
    {
    	itera_grid_cor[i][0] = valid_grid[i][0];
    	itera_grid_cor[i][1] = valid_grid[i][1];
    	itera_grid_cor[i][2] = valid_grid[i][2];
    }// first iteration


//    for (i = 0; i < reW_num; i++) {
//      printf("second: %d\n",leswat[i]);}


//    for (i = 0; i < valid_count; i ++)
//    {
//	printf("first test %f %f %f\n",itera_grid_cor[i][0],itera_grid_cor[i][1],itera_grid_cor[i][2]);
//    }   

//    for (i = 0; i < reW_num; i++) {
//      printf("third: %d\n",leswat[i]);}

    for (i = 0; i < itera_num; i++)
    {
	for (j = 0; j < valid_count; j++)
	{       
		sumx = 0;
		sumy = 0;
		sumz = 0;
		avex = 0;
		avey = 0;
		avez = 0;
		iter_temp_count = 0;		
		for (k = 0; k < reW_num; k++)
		{
            h = leswat[k];

            if (sqrt(pow(mywat[h].x-valid_grid[j][0],2)+
				 pow(mywat[h].y-valid_grid[j][1],2)+
				 pow(mywat[h].z-valid_grid[j][2],2)) < 0.6 )
			{
				iter_temp_count +=1;
				sumx = sumx + mywat[h].x;
				sumy = sumy + mywat[h].y;
				sumz = sumz + mywat[h].z;
			}
		}
		avex = sumx / iter_temp_count;
		avey = sumy / iter_temp_count;
		avez = sumz / iter_temp_count;
		itera_grid_cor[j][0] = avex;
		itera_grid_cor[j][1] = avey;
		itera_grid_cor[j][2] = avez;
	}
    }
//  print water site
    for (i = 0; i < valid_count; i ++)
    {
	printf("%f %f %f\n",itera_grid_cor[i][0],itera_grid_cor[i][1],itera_grid_cor[i][2]);
    }
		
    return 0;
}