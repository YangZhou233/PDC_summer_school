// A try for integration
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h> 
#include <memory.h>
#include "typstruct.h"

static float GRID_SIZE = 0.12; //nm

//int rpdb(char * filename, int * atomnum, int * watnum, ATOM * atom);
//int rxtc(char * filename, int atomnum, int watnum, ATOM * atom, awt * mywat, vt * boxct, int * frames, int * len_awt);


int findwat(int len_awt, awt * mywat, int watnum, int frames)
{

   float distance2(float x1, float y1, float z1, float x2, float y2, float z2)
   { /*
     function to return the distance^2 of two given x,y,z
     */
     float dis;
     dis = (x1 - x2)*(x1 - x2) + (y1 - y2)*(y1 - y2) + (z1 - z2)*(z1 - z2);
     return dis;
   }

   void mergeCloseGrid(float site[][4], int s)
   { 

   }
   

   int h, i, j, x, y, z;
   int reW_num = 0;
   lt * leswat;
   leswat = (lt *) malloc(len_awt * sizeof(lt));
   //int leswat[len_awt];
   //memset(leswat,len_awt,sizeof leswat);
   //int count[20][20][20];
   int count[20][20][20] = {0};
   int grid_num = (0.8+0.12)/0.12; //grid number in 1D
   // int grid_num = 12; // within 0.72 nm of the specific point, 6 grids alone the radius and 12 grids in total along the diameter
   float grid_edge_radius = GRID_SIZE / 2.0;
   float grid_edge_min[2];
   int max_count=0;
   int max_count_id[3];
   int valid_count=0;
   int temp_count = 0;
   int itera_num = 2, k, iter_temp_count;
   float valid_grid[1000][4];
   float sumx, sumy, sumz, avex, avey, avez;
   float itera_grid_cor[1000][3];


// reduce water, box center 2 2 2
   
   for (i = 1; i < len_awt; i++)
   {
      if (mywat[i].x <5 && mywat[i].y<50 && mywat[i].z<5)
      {
         //leswat[reW_num] = i;
         //printf("index and wat xyz: %d %f %f %f\n", i, mywat[i].x, mywat[i].y, mywat[i].z);
         (leswat+reW_num)->num = i;
         //printf("The stored list is %d\n", (leswat+reW_num)->num);
         reW_num +=1;
	}
    }

    //printf("reW_num is %d\n\n", reW_num);  
  
    grid_edge_min[0] = 2.38-grid_num/2*GRID_SIZE; //grid size 0.12 nm
    grid_edge_min[1] = 2.92-grid_num/2*GRID_SIZE;
    grid_edge_min[2] = 2.54-grid_num/2*GRID_SIZE;

    
   for (i = 0; i < reW_num; i++) {
      //j = leswat[i];
      j = (leswat+i)->num;
      //printf("i(reW_num) is %d, J is %d\n", i, j);
      for (x = 0; x < grid_num; x++) {
         if (mywat[j].x>(grid_edge_min[0]+x*GRID_SIZE) && mywat[j].x<(grid_edge_min[0]+(x+1)*GRID_SIZE)) {
            for (y = 0; y < grid_num; y++) {
               if (mywat[j].y>(grid_edge_min[1]+y*GRID_SIZE) && mywat[j].y<(grid_edge_min[1]+(y+1)*GRID_SIZE)) {
                  for (z = 0; z < grid_num; z++) {
                     if (mywat[j].z>(grid_edge_min[2]+z*GRID_SIZE) && mywat[j].z<(grid_edge_min[2]+(z+1)*GRID_SIZE)) {
                        count[x][y][z] +=1;
                     }
                  }
               }
            }
         }
      }
   }
   
// testing print the max count

   for (x = 0; x < grid_num; x++) {
      for (y = 0; y < grid_num; y++) {
         for (z = 0; z < grid_num; z++) {
            if (count[x][y][z]>max_count) {
               max_count=count[x][y][z];
               max_count_id[0]=x;
               max_count_id[1]=y;
               max_count_id[2]=z;
               printf("max count: %d in grid: %f %f %f\n", max_count, x*GRID_SIZE+grid_edge_min[0]+grid_edge_radius,
               y*GRID_SIZE+grid_edge_min[1]+grid_edge_radius,
               z*GRID_SIZE+grid_edge_min[2]+grid_edge_radius);               
            }


            if (count[x][y][z]>1000) {
                              valid_count +=1;
               valid_grid[temp_count][0]=grid_edge_min[0]+x*GRID_SIZE + grid_edge_radius;
               valid_grid[temp_count][1]=grid_edge_min[1]+y*GRID_SIZE + grid_edge_radius;
               valid_grid[temp_count][2]=grid_edge_min[2]+z*GRID_SIZE + grid_edge_radius;
               valid_grid[temp_count][3]=count[x][y][z];
               temp_count +=1;
            }
         }
      }
   }


   //for (x = 0; x < grid_num; x++) {
   //   for (y = 0; y < grid_num; y++) {
   //      for (z = 0; z < grid_num; z++) {
   //         if (count[x][y][z]>100) {
   //            valid_count +=1;
   //         }
   //      }
   //   }
   //}
    

   // for (x = 0; x < grid_num; x++)
   // {
   //   for (y = 0; y < grid_num; y++)
   //   {
   //           for (z = 0; z < grid_num; z++)
   //           {
   //                   if (count[x][y][z]>100)
   //                   {
   //                     valid_grid[temp_count][0]=grid_edge_min[0]+x*0.1;
   //     		valid_grid[temp_count][1]=grid_edge_min[1]+y*0.1;
   //     		valid_grid[temp_count][2]=grid_edge_min[2]+z*0.1;
   //     		valid_grid[temp_count][3]=count[x][y][z];
   //     		temp_count +=1;
   //                   }
   //           }
   //   }
   // }
    
    //printf("valid_count grid_num: %d %d\n", valid_count, grid_num);
    for (i = 0; i < valid_count; i++)
    {
	printf("valid counts:%f in %f %f %f\n",valid_grid[i][3],valid_grid[i][0],valid_grid[i][1],valid_grid[i][2]);
    }
  
    //itera_grid_cor[88][3]=valid_grid[88][3]; // first iteration
    
    for (i = 0; i < valid_count; i++)
    {
        itera_grid_cor[i][0] = valid_grid[i][0];
        itera_grid_cor[i][1] = valid_grid[i][1];
        itera_grid_cor[i][2] = valid_grid[i][2];
    }// first iteration



float site[1000][4];
int counter;
    for (i = 0; i < itera_num; i ++)
    { // merge grids if two center distance is less than 0.1 nm  
        for (i = 0; i < valid_count; i++)
        {
               site[i][0]= itera_grid_cor[i][0];
               site[i][1]= itera_grid_cor[i][1];
               site[i][2]= itera_grid_cor[i][2];
               site[i][3]= 0.0;
        }
        
        for (i = 0; i < valid_count; i++)
        {
            if ( site[i][3] == 0.0 )
            {
              counter = 1;
              x = site[i][0];
              y = site[i][1];
              z = site[i][2];
              site[i][3]= 1.0;
              for (j = 0; j < valid_count; j++)
              {
                if (distance2(site[i][0], site[i][1], site[i][2], site[j][0], site[j][1], site[j][2])<0.01)
                {
                  x += site[j][0];
                  y += site[j][1];
                  z += site[j][2];
                  counter += 1;
                  site[j][3] = 1.0;
                }
              }
              x/=counter;
              y/=counter;
              z/=counter;
              itera_grid_cor[i][0]=x;
              itera_grid_cor[i][1]=y;
              itera_grid_cor[i][2]=z;
            }
        }
        
	for (j = 0; j < valid_count; j++)
	{       
		sumx = 0;
		sumy = 0;
		sumz = 0;
		avex = 0;
		avey = 0;
		avez = 0;
		iter_temp_count = 0;

		for (k = 0; k < reW_num; k++)
		{
                   //h = leswat[k];
                   h = (leswat+k)->num;
		   //if (j==0) {
		     //printf("The h is %d\n",h);
		   //}
			if (sqrt(pow(mywat[h].x-valid_grid[j][0],2)+
				 pow(mywat[h].y-valid_grid[j][1],2)+
				 pow(mywat[h].z-valid_grid[j][2],2)) < 0.13 )
			{
				iter_temp_count +=1;
				sumx = sumx + mywat[h].x;
				sumy = sumy + mywat[h].y;
				sumz = sumz + mywat[h].z;
			}
		}
		avex = sumx / iter_temp_count;
		avey = sumy / iter_temp_count;
		avez = sumz / iter_temp_count;
		itera_grid_cor[j][0] = avex;
		itera_grid_cor[j][1] = avey;
		itera_grid_cor[j][2] = avez;
	}
    }
//  print water site
    for (i = 0; i < valid_count; i ++)
    {
	printf("HETATM%5d O    H2O%6d      %5.3f  %5.3f  %5.3f %4.3f   %2.1f           O-1\n",i,i,itera_grid_cor[i][0]*10,itera_grid_cor[i][1]*10,itera_grid_cor[i][2]*10,1.0,1.0);
    }
			
   free(leswat);
   leswat = NULL; // to avoid the pointer points to other places!
   return 0;
}
