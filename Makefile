CC = gcc
CFLAGS = -O2 -c -g -Wall
LDFLAGS =
LIBFLAGS = -lm -fopenmp
TARGETNAME = "hydra_site.x"

SOURCES = input.c calc_func.c main_func.c xdrfile.c xdrfile_xtc.c

OBJECTS = $(SOURCES:.c=.o)

all: $(TARGETNAME)

$(TARGETNAME): $(OBJECTS)
	$(CC) $(LDFLAGS) $(OBJECTS) $(LIBFLAGS) -o $@

.c.o:
	$(CC) $(CFLAGS) $< $(LIBFLAGS) -o $@

clean:
	/bin/rm -f $(OBJECTS) $(TARGETNAME)
