/*
 * PDC Summer School 2018
 *
 * Project member: Rongfeng Zou, Yang Zhou and Junhao Li
 *
 * To compile the code, just type: make all
 *
 * Remember to "make clean" before re-compiling the code.
 *
 */

#include <stdio.h>
#include <stdlib.h>
//#include <time.h>
#include "typstruct.h" 

int rpdb(char * filename, int * atomnum, int * watnum, ATOM * atom);
int rxtc(char * filename, int atomnum, int watnum, ATOM * atom, awt * mywat, vt * boxct, int * frames, int * len_awt);
int checkout(int len_awt, awt * mywat, vt * boxct);
int findwat(int len_awt, awt * mywat, lt * leswat, int * les_num, int watnum, int frames, float (*init_grid)[5], int * init_count, float box_center[3], float box_size[3]);
int merge(float (*init_grid)[5], int init_count, float (*merge_grid)[5], int * merge_count);
int iteration(awt * mywat, float (*init_grid)[5], int init_count, lt * leswat, int les_num, int frames);


int main(int argc, char *argv[])
{
   if (argc < 3) {
      printf("Error, please input the topology(pdb) and trajectory(xtc)\n");
      exit(1);
   }

   int atomnum, watnum, frames, len_awt, les_num, init_count;
   float t_star, t_end1, t_end2;
   float init_grid[600][5] = {0};
   vt  boxct;
   ATOM * atom;
   awt  * mywat;
   lt   * leswat;
   atom   = (ATOM *) malloc(sizeof(ATOM) * MAXATOM);
   mywat  = (awt *) malloc(sizeof(awt) * MAXWAT);
   t_star = omp_get_wtime();

   rpdb(argv[1], &atomnum, &watnum, atom);
   if (DEBUG == 1) {
      printf("\natoms %d; water %d\n", atomnum, watnum);
      printf("omp_get_num_threads() is %d\n", omp_get_num_threads());
   }
   rxtc(argv[2], atomnum, watnum, atom, mywat, &boxct, &frames, &len_awt);
   if (DEBUG == 1) {
      printf("\nFrames: %d ;box center: %lf %lf %lf; num of all water %d\n", frames,  boxct.v1, boxct.v2, boxct.v3, len_awt);
   } 
   t_end1 = omp_get_wtime();
   printf("\nTime duration for input_reading: %f s\n", t_end1 - t_star);

   leswat = (lt *) malloc(len_awt * sizeof(lt));
   float box_center[3] = {boxct.v1, boxct.v2, boxct.v3};
   float box_size[3] = {2*boxct.v1-0.1, 2*boxct.v2-0.1, 2*boxct.v3-0.1}; // box size is smaller (-0.1nm) than the simulation box
   if (DEBUG == 2) {
      printf("awt_len is %d, watnum * frames is %d\n", len_awt, watnum * (frames - 1));
      checkout(len_awt, mywat, &boxct);
   }
   
   findwat(len_awt, mywat, leswat, &les_num, watnum, frames, init_grid, &init_count, box_center, box_size);
   t_end2 = omp_get_wtime();
   printf("\nTime duration for findwat(): %f s\n", t_end2 - t_end1);
   
   iteration(mywat, init_grid, init_count, leswat, les_num, frames);
   t_end1 = omp_get_wtime();
   printf("\nTime duration for iteration(): %f s\n", t_end1 - t_end2);
   printf("\nTOTAL running Time duration: %f s (%f min)\n", t_end1 - t_star, (t_end1 - t_star)/60.0);
   
   free(atom);
   free(mywat);
   free(leswat);
   atom = NULL;
   mywat = NULL;
   leswat = NULL;
   return 0;
}
