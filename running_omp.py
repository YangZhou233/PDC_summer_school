#!/usr/bin/env python
import os
import sys

npt = ' ' + sys.argv[1] + ' '
xtc = ' ' + sys.argv[2] + ' '
omp = 11

f = open('Timing.dat', 'w')
f.write('#OMP_NUM_THREADS, Sp, Ep, Tp, input_reading, findwat, TOTAL\n')
result = []

def gettime(out):
   out2 = []
   for line in out:
      tmp = line.split()
      if 'input_reading:' in tmp:
         out2.append(tmp[4])  # input_reading
      elif 'findwat():' in tmp:
         out2.append(tmp[4])  # findwat()
      elif 'iteration():' in tmp:
         out2.append(tmp[4])  # iteration()
      elif 'TOTAL' in tmp:
         out2.append(tmp[4])
   return(out2)

#f = open('Timing.dat', 'a')
for i in range(1, omp):
   os.environ["OMP_NUM_THREADS"] = str(i)
   out   = os.popen("./hydra_site.x" + npt + xtc).readlines()
   os.system('mv WatSites.pdb WatSites_omp' + str(i) + '.pdb')
   times = gettime(out)

   if i == 1:
      T1 = float(times[2])
      Tp = float(times[2])
      Sp = T1/Tp
      Ep = T1/(i*Tp)
   else:
      Tp = float(times[2])
      Sp = T1/Tp
      Ep = T1/(i*Tp)
   rt = times[0]
   ft = times[1]
   tt = times[3]
   result.append([i, Sp, Ep, Tp, rt, ft, tt])
#   print(result)

f.write('\n'.join([' '.join([str(j) for j in x]) for x in result]))
f.close()
